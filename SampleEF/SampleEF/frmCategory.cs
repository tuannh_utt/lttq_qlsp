﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SampleEF
{
    public partial class frmCategory : Form
    {
        private SAMPLEEntities sampleContext = new SAMPLEEntities();
        public frmCategory()
        {
            InitializeComponent();
        }

        public void LoadDataToDgv()
        {
            var result = sampleContext.Categories.ToList();
            dgvCategory.DataSource = result;

            dgvCategory.AutoGenerateColumns = false;

            dgvCategory.Columns[0].HeaderText = "Mã loại";
            dgvCategory.Columns[1].HeaderText = "Tên loại";
            dgvCategory.Columns[2].HeaderText = "Mô tả";
        }

        private void frmCategory_Load(object sender, EventArgs e)
        {
            LoadDataToDgv();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            txtCategoryDescription.Text = "";
            txtCategoryId.Text = "";
            txtCategoryName.Text = "";

            btnEdit.Visible = false;
            btnDelete.Visible = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //kiểm tra dữ liệu nhập
            if (txtCategoryName.Text == "")
            {
                MessageBox.Show("Bạn cần phải nhập tên loại sản phẩm.");
                txtCategoryName.Focus();
                return;
            }

            var category = new Category
            {
                CategoryName = txtCategoryName.Text,
                Description = txtCategoryDescription.Text
            };
            //thêm đối tượng Category 
            sampleContext.Categories.Add(category);

            //lưu lại thay đổi vào cơ sở dữ liệu 
            sampleContext.SaveChanges();

            LoadDataToDgv();

            btnEdit.Visible = true;
            btnDelete.Visible = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int id;
            if (int.TryParse(txtCategoryId.Text, out id) != true)
            {
                MessageBox.Show("Bạn cần phải chọn loại sản phẩm.");
                return;
            }
            //tìm bản ghi cần xoá
            var category = sampleContext.Categories.Find(id);


            //hỏi xem người dùng có chắc chắn xoá hay không


            //thực hiện xoá và lưu lại
            sampleContext.Categories.Remove(category);
            sampleContext.SaveChanges();

            LoadDataToDgv();
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            txtCategoryName.ReadOnly = false;
            txtCategoryDescription.ReadOnly = false;

            //kiểm tra dữ liệu nhập
            if (txtCategoryName.Text == "")
            {
                MessageBox.Show("Bạn cần phải nhập tên loại sản phẩm.");
                txtCategoryName.Focus();
                return;
            }

            int id;
            if (int.TryParse(txtCategoryId.Text, out id)!=true)
            {
                MessageBox.Show("Bạn cần phải chọn loại sản phẩm.");
                return;
            }

            btnAdd.Visible = false;
            btnDelete.Visible = false;

            //tìm đối tượng để sửa thông tin
            var category = sampleContext.Categories.Find(id);

            //thay đổi lại thông tin cho đối tượng cần sửa
            category.CategoryName = txtCategoryName.Text;
            category.Description = txtCategoryDescription.Text;

            sampleContext.SaveChanges();

            LoadDataToDgv();

        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void dgvCategory_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //lấy ra chỉ số hàng được chọn
            int index = e.RowIndex;

            //đưa các thông tin lên các ô textbox
            txtCategoryId.ReadOnly = true;
            txtCategoryId.Text = dgvCategory.Rows[index].Cells[0].Value.ToString();
            txtCategoryName.Text = dgvCategory.Rows[index].Cells["CategoryName"].Value.ToString();
            txtCategoryDescription.Text = dgvCategory.Rows[index].Cells[2].FormattedValue.ToString();

            txtCategoryId.ReadOnly = true;
            txtCategoryName.ReadOnly = true;
            txtCategoryDescription.ReadOnly = true;
        }
    }
}
