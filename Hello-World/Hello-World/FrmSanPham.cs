﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Hello_World.Model;

namespace Hello_World
{
    public partial class FrmSanPham : Form
    {
        public static List<DanhMuc> LstDanhMuc = new List<DanhMuc>();
        List<SanPham> LstSanPham = new List<SanPham>();

        public FrmSanPham()
        {
            InitializeComponent();
        }

        private void btnThemDm_Click(object sender, EventArgs e)
        {
            FrmDanhMuc frmDm = new FrmDanhMuc();
            frmDm.ShowDialog();
        }

        private void btnThemSp_Click(object sender, EventArgs e)
        {
            txtMaSp.Clear();
            txtTenSp.Clear();
            txtMoTa.Clear();
            txtGiaNhap.Clear();
            txtSoLuong.Clear();
            dtpNgayNhap.Value = DateTime.Now;
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            //kiểm tra đã chọn danh mục hay chưa
            if (cbbDanhMuc.SelectedIndex==-1)
            {
                MessageBox.Show("Cần phải chọn danh mục cho sản phẩm");
                return;
            }
            DanhMuc dm = cbbDanhMuc.SelectedItem as DanhMuc;

            //kiểm tra số lượng
            int sl;
            var kt = int.TryParse(txtSoLuong.Text, out sl);
            if (kt==false)
            {
                MessageBox.Show("Cần phải nhập số nguyên");
                txtSoLuong.Clear();
                txtSoLuong.Focus();
                return;
            }

            //kiểm tra đơn giá
            double dg;
            kt = double.TryParse(txtSoLuong.Text, out dg);
            if (kt == false)
            {
                MessageBox.Show("Cần phải nhập số");
                txtGiaNhap.Clear();
                txtGiaNhap.Focus();
                return;
            }

            //kiểm tra ngày nhập
            if (dtpNgayNhap.Value > DateTime.Now)
            {
                MessageBox.Show("Ngaày nhập không được lớn hơn ngày hiện tại");
                return;
            }
            //kiểm tra mã sản phẩm

            //thêm sản phẩm vào danh sách sản phẩm
            SanPham sp = new SanPham
            {
                MaSanPham = txtMaSp.Text,
                TenSaPham = txtTenSp.Text,
                MoTa = txtMoTa.Text,
                Gia = dg,
                SoLuong = sl,
                NgayNhap = dtpNgayNhap.Value,
                Nhom = dm
            };
            dm.ThemSanPham(sp);
            LstSanPham.Add(sp);
            btnThemSp_Click(sender, e);
            LoadSanPhamLenListBox();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (lsbSp.SelectedIndex==-1)
            {
                MessageBox.Show("Cần chọn sản phẩm để xoá");
                return;
            }
            SanPham sp = lsbSp.SelectedItem as SanPham;
            DialogResult res = MessageBox.Show("Chắn chắn xoá " + sp.TenSaPham + "không?",
                "Xoá sản phẩm", MessageBoxButtons.YesNo);
            if (res==DialogResult.Yes)
            {
                LstSanPham.Remove(sp);
            }

            LoadSanPhamLenListBox();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lsbSp_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lsbSp.SelectedIndex!=-1)
            {
                SanPham sp = lsbSp.SelectedItem as SanPham;
                txtMaSp.Text = sp.MaSanPham;
                txtTenSp.Text = sp.TenSaPham;
                txtMoTa.Text = sp.MoTa;
                txtGiaNhap.Text = sp.Gia.ToString();
                txtSoLuong.Text = sp.SoLuong.ToString();
                dtpNgayNhap.Value = sp.NgayNhap;
            }
        }

        private void LoadSanPhamLenListBox()
        {
            lsbSp.Items.Clear();
            foreach (var item in LstSanPham)
            {
                lsbSp.Items.Add(item);
            }
        }

        private void LoadDanhMucLenCombobox()
        {
            cbbDanhMuc.Items.Clear();
            foreach (var item in LstDanhMuc)
            {
                cbbDanhMuc.Items.Add(item);
            }
            cbbDanhMuc.DisplayMember = "TenDanhMuc";
            cbbDanhMuc.ValueMember = "MaDanhMuc";
        }

        private void FrmSanPham_Load(object sender, EventArgs e)
        {
            DanhMuc dm1 = new DanhMuc
            {
                MaDanhMuc = "1",
                TenDanhMuc = "Máy tính",
                MoTa = ""
            };
            DanhMuc dm2 = new DanhMuc
            {
                MaDanhMuc = "2",
                TenDanhMuc = "Điện thoại",
                MoTa = ""
            };
            DanhMuc dm3 = new DanhMuc
            {
                MaDanhMuc = "3",
                TenDanhMuc = "Bàn ghế",
                MoTa = ""
            };
            LstDanhMuc.Add(dm1);
            LstDanhMuc.Add(dm2);
            LstDanhMuc.Add(dm3);
            LoadDanhMucLenCombobox();
        }
    }
}
