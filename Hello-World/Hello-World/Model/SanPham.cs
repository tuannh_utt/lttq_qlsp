﻿using System;

namespace Hello_World.Model
{
    public class SanPham
    {
        public string MaSanPham { get; set; }
        public string TenSaPham { get; set; }
        public string MoTa { get; set; }
        public int SoLuong { get; set; }
        public double Gia { get; set; }
        public DateTime NgayNhap { get; set; }

        public DanhMuc Nhom { get; set; }

        public override string ToString()
        {
            return TenSaPham;
        }
    }
}
