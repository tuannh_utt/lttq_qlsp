﻿using System.Collections.Generic;

namespace Hello_World.Model
{
    public class DanhMuc
    {
        public string MaDanhMuc { get; set; }
        public string TenDanhMuc { get; set; }
        public string MoTa { get; set; }

        private Dictionary<string, SanPham> _listSanPhams
            = new Dictionary<string, SanPham>();

        public void ThemSanPham(SanPham sp)
        {
            if (_listSanPhams.ContainsKey(sp.MaSanPham) == false)
            {
                _listSanPhams.Add(sp.MaSanPham, sp);
                sp.Nhom = this;
            }
        }

        public Dictionary<string, SanPham> SanPhams
        {
            get { return _listSanPhams; }
            set { _listSanPhams = value; }
        }

        public override string ToString()
        {
            return TenDanhMuc;
        }
    }
}
