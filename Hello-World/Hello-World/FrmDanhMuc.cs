﻿using System;
using System.Windows.Forms;
using Hello_World.Model;

namespace Hello_World
{
    public partial class FrmDanhMuc : Form
    {
        public FrmDanhMuc()
        {
            InitializeComponent();
        }

        private void lsbDm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lsbDm.SelectedIndex != -1)
            {
                DanhMuc dm = lsbDm.SelectedItem as DanhMuc;
                txtMaDm.Text = dm.MaDanhMuc;
                txtTenDm.Text = dm.TenDanhMuc;
                txtMoTa.Text = dm.MoTa;
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            txtMaDm.Text = "";
            txtTenDm.Text = "";
            txtMoTa.Text = "";
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            DanhMuc dm = new DanhMuc
            {
                MaDanhMuc = txtMaDm.Text,
                TenDanhMuc = txtTenDm.Text,
                MoTa = txtMoTa.Text
            };

            FrmSanPham.LstDanhMuc.Add(dm);
            LoadDanhMucLenListBox();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (lsbDm.SelectedIndex!=-1)
            {
                DanhMuc dm = lsbDm.SelectedItem as DanhMuc;
                FrmSanPham.LstDanhMuc.Remove(dm);
            }

            LoadDanhMucLenListBox();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadDanhMucLenListBox()
        {
            lsbDm.Items.Clear();
            foreach (var item in FrmSanPham.LstDanhMuc)
            {
                lsbDm.Items.Add(item);
            }
        }

        private void FrmDanhMuc_Load(object sender, EventArgs e)
        {
            LoadDanhMucLenListBox();
        }
    }
}
